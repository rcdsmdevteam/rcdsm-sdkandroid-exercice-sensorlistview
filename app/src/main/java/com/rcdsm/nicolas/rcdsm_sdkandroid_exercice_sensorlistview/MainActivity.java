package com.rcdsm.nicolas.rcdsm_sdkandroid_exercice_sensorlistview;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements SensorEventListener {

	protected SensorManager sensorManager;
	protected ListView lv;
	protected ArrayList<SensorInfo> sensorsList;
	protected SensorAdapter sa;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lv = (ListView) findViewById(R.id.listView);

		sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
		List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);

		sensorsList = new ArrayList<SensorInfo>();

		for (Sensor sensor : sensors) {
			sensorsList.add(new SensorInfo(sensor));
			sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		}

		sa = new SensorAdapter(this, sensorsList);

		lv.setAdapter(sa);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		for(SensorInfo sensor : sensorsList){
			if(event.sensor.getName().equals(sensor.getSensor().getName())){
				sensor.setLastValues(event.values);
			}
		}
		sa.notifyDataSetChanged();

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}
}
