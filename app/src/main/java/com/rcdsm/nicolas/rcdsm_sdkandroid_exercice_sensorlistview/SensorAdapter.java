package com.rcdsm.nicolas.rcdsm_sdkandroid_exercice_sensorlistview;

import android.content.Context;
import android.hardware.Sensor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Nicolas on 12/8/2014.
 */
public class SensorAdapter extends BaseAdapter{
	protected ArrayList<SensorInfo> sensors;
	protected View itemView;
	protected Context context;
	protected TextView tvSensorName;
	protected TextView tvSensorData;

	public SensorAdapter(Context context, ArrayList<SensorInfo> sensors) {
		this.context = context;
		this.sensors = sensors;
		this.itemView = null;
	}

	@Override
	public int getCount() {
		return sensors.size();
	}

	@Override
	public Object getItem(int position) {
		return sensors.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//On récupère un fichier à inflater
		LayoutInflater inflater = (LayoutInflater.from(this.context));

		//On inflate le fichier XML pour créé les composants de la vue
		itemView = inflater.inflate(R.layout.listview_item, null);

		//On récupère le sensor en cour de traitement
		SensorInfo currentSensor = this.sensors.get(position);

		//On récupère les composants de la vue
		tvSensorName = (TextView) itemView.findViewById(R.id.tvSensorName);
		tvSensorData = (TextView) itemView.findViewById(R.id.tvSensorData);


		//On met à jour les composants précédement récupérés à partir des données de la personne n° position
		tvSensorName.setText(currentSensor.getSensor().getName());
		tvSensorData.setText(currentSensor.getLastValuesString());

		return this.itemView;
	}
}
