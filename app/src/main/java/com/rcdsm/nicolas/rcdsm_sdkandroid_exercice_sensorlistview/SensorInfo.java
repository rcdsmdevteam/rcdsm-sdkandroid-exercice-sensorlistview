package com.rcdsm.nicolas.rcdsm_sdkandroid_exercice_sensorlistview;

import android.hardware.Sensor;

/**
 * Created by Nicolas on 12/8/2014.
 */
public class SensorInfo {
	protected Sensor sensor;
	protected float[] lastValues;

	public Sensor getSensor() {
		return sensor;
	}

	public String getLastValuesString() {
		String printedValues = "/ ";

		if(lastValues != null){
			for(float value : lastValues){
				printedValues += value + " / ";
			}
		}

		return printedValues;
	}

	public void setLastValues(float[] lastValues) {
		this.lastValues = lastValues;
	}

	public SensorInfo(Sensor sensor) {
		this.sensor = sensor;
	}
}
